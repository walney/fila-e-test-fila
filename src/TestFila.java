import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import junit.framework.Assert;

public class TestFila {
	@Test
	public void enqueueTeste0() {
		Fila fil=new Fila();
		fil.enqueue(2);
		fil.enqueue(3);
		assertEquals(2, fil.front());
		
	}
	@Test
	public void enqueueTeste1() {
		Fila fil=new Fila();
		fil.enqueue(42);
		fil.enqueue(3);
		fil.enqueue(3);
		fil.enqueue(3);
		
		assertEquals(42, fil.front());
	}
	
	@Test
	public void enqueueTeste2() {
		Fila fil=new Fila();
		fil.enqueue(42);
		fil.enqueue(3);
		fil.enqueue(4);
		fil.enqueue(3);
		fil.enqueue(4);
		fil.enqueue(3);
		assertEquals(42, fil.front());

		
	}
	@Test
	public void enqueueTeste3() {
		Fila fil=new Fila();
		fil.enqueue(3);
		fil.enqueue(3);
		fil.enqueue(3);
		fil.enqueue(3);
		fil.enqueue(3);
		fil.enqueue(3);
		fil.enqueue(3);
		fil.enqueue(3);
		
		assertEquals(3, fil.front());
}
	@Test
	public void dequeueTeste0() {
		Fila fil=new Fila();
		
		fil.enqueue(4222222);
		fil.enqueue(3);
		
		assertEquals(4222222, fil.dequeue());
}
	@Test
	public void dequeueTeste1() {
		Fila fil=new Fila();
		
		
		fil.enqueue(3);
		fil.enqueue(3);
		fil.enqueue(3);
		fil.enqueue(3);
		fil.enqueue(3);
		fil.enqueue(3);
		
		assertEquals(3, fil.dequeue());
}
	
	@Test
	public void isempytTeste1() {
		Fila fil=new Fila();
		fil.enqueue(444);
		
		
		assertEquals(1, fil.isEmpty());
}
	@Test
	public void frontTeste1() {
		Fila fil=new Fila();
		fil.enqueue(444);
		fil.enqueue(444);
		fil.enqueue(4);
		fil.enqueue(4);
		
		assertEquals(444, fil.front());
	}
	@Test
	public void isempytTeste2() {
		Fila fil=new Fila();
		fil.enqueue(444);
		fil.enqueue(444);
		fil.enqueue(444);
		fil.enqueue(444);
		fil.enqueue(444);
		fil.enqueue(444);
		fil.enqueue(444);
		fil.enqueue(444);
		
		
		
		assertEquals(8, fil.isEmpty());
}
	@Test
	public void frontTeste2() {
		Fila fil=new Fila();
		fil.enqueue(444);
		fil.enqueue(444);
		fil.enqueue(444);
		fil.enqueue(444);
		fil.enqueue(444);
		fil.enqueue(444);
		fil.enqueue(444);
		fil.enqueue(444);
		
		
		
		assertEquals(444, fil.front());
}
	
	
}
